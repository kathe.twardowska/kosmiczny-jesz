﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BadassEnemy : Enemy
{
    [SerializeField] Transform arm;
    [SerializeField] float asteroidSpeed;
    [SerializeField] ParticleSystem dieParticle;

    protected override void Start()
    {
        base.Start();
        State = EnemyState.FollowTarget;
    }

    protected override void AttackTarget()
    {
        var rotation = Quaternion.Euler(Random.Range(0, 360f), Random.Range(0, 360f), Random.Range(0, 360f));
        var bullet = ObjectPooler.instance.SpawnFromPool(Random.Range(0f,1f) < 0.5f ? "Asteroida" : "Asteroida2", arm.position, rotation);
        bullet.GetComponent<Rigidbody>().velocity = (Target.position - arm.position).normalized * asteroidSpeed;
    }

    protected override void Die()
    {
        base.Die();
        var particle = Instantiate(dieParticle, transform.position, Quaternion.identity);
        particle.transform.localScale = Vector3.one * 2;
        CameraShake.instance.Shake(.25f, .4f);
    }
}
