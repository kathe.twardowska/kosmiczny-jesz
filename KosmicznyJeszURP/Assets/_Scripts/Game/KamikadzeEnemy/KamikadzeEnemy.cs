﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KamikadzeEnemy : Enemy
{
    protected override void Start()
    {
        base.Start();
        State = EnemyState.FollowTarget;
    }

    protected override void AttackTarget()
    {
        //particle
        var playerStats = Target.GetComponent<Stats>();
        playerStats.SubstractHealth(GetDamage());
        gameObject.SetActive(false);
    }
}
