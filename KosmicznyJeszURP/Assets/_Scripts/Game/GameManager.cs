﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class GameManager : MonoBehaviour
{
    InputController _input;
    JeszSpinningControll _jeszSpinningControll;
    public GameObject MeshDisplayHandler;
    public GameObject Arrow;
    public Animator StartTextAnimator;
    public Animator AppereanceAnimator;
    public bool isGamplayStarted;

    public void OnDisactive()
    {
        _input.enabled = false;
        _jeszSpinningControll.enabled = false;
        MeshDisplayHandler.SetActive(false);
        Arrow.SetActive(false);
    }

    public void OnActive()
    {
        _input.enabled = true;
        _jeszSpinningControll.enabled = true;
        MeshDisplayHandler.SetActive(true);
        Arrow.SetActive(true);
        GetComponent<EnemySpawner>().enabled = true;
        GetComponent<DillSpawner>().enabled = true;
        GetComponent<AsteroidSpawner>().enabled = true;
    }

    void OnGameplayEnd()
    {
        AppereanceAnimator.SetTrigger("WhiteToBlack");
    }

    void Start()
    {
        _input = GetComponent<InputController>();
        _jeszSpinningControll = GetComponent<JeszSpinningControll>();
        OnDisactive();
        AppereanceAnimator.SetTrigger("BlackToWhite");
        StartTextAnimator.SetTrigger("Play");
        LockCursor();
    }

    private void LockCursor()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    private void Update()
    {
        if (Input.anyKey)
        {
            LockCursor();
        }
        if (Input.anyKeyDown && !isGamplayStarted)
        {
            StartTextAnimator.SetTrigger("Stop");
            OnActive();
            isGamplayStarted = true;
        }
    }
}
