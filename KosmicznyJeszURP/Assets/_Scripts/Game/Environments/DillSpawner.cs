﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DillSpawner : MonoBehaviour
{
    [SerializeField] float distance;
    [SerializeField] float cooldown;

    private float _cooldownCounter;

    private int _dillCount;

    private void Update()
    {
        _dillCount = FindObjectsOfType<Dill>().Length;

        print(_dillCount);

        if(_dillCount == 0)
            _cooldownCounter += Time.deltaTime;

        if (_cooldownCounter < cooldown)
            return;
        var position = transform.position + new Vector3(Random.Range(-1, 1f), Random.Range(-1, 1f), Random.Range(-1, 1f)).normalized * distance;
        ObjectPooler.instance.SpawnFromPool("Dill", position, Quaternion.identity);
        _cooldownCounter = 0;
    }
}
