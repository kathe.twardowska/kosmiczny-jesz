﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dill : Consumable
{
    [SerializeField] float healthRegen;

    public override void Eat(Stats player)
    {
        base.Eat(player);
        player.AddHealth(healthRegen);
    }
}
