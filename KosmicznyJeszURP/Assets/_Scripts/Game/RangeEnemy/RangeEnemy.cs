﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeEnemy : Enemy
{
    [SerializeField] float timeToDash;
    [SerializeField] float animationSpeed;
    [SerializeField] float bulletSpeed;
    [SerializeField] Transform arm;

    private Vector3 _dashDirection;
    private int _ammoCounter;

    protected override void Start()
    {
        base.Start();
        State = EnemyState.FollowTarget;
        RunFromTarget = true;
        InvokeRepeating("UseAbility", timeToDash, timeToDash);
        StartCoroutine(ShootBulletLoopEnum());
    }

    protected override void AttackTarget()
    {
        _ammoCounter = 7;
    }

    IEnumerator ShootBulletLoopEnum()
    {
        yield return new WaitForSeconds(0.1f);

        if (_ammoCounter > 0)
            ShootBullet();

        StartCoroutine(ShootBulletLoopEnum());
    }

    private void ShootBullet()
    {
        var _shootDirection = (Target.position - arm.position).normalized;
        ObjectPooler.instance.SpawnFromPool("LaserBall", arm.position, Quaternion.identity).GetComponent<Rigidbody>().velocity = _shootDirection * bulletSpeed;
        _ammoCounter--;
    }

    protected override void IdleAnimation()
    {
        if (GetComponent<Dash>().IsDashing())
        {
            _dashDirection = (transform.up * Random.Range(-1f, 1f) + transform.right * Random.Range(-1f, 1f)).normalized;
            return;
        }

        transform.Translate(_dashDirection * Time.deltaTime * animationSpeed);
    }
}
