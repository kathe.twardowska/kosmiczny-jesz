﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserBulletInstance : MonoBehaviour
{
    public float Damage;
    [SerializeField] LayerMask destroyingMask;

    private void OnTriggerEnter(Collider other)
    {
        var player = other.gameObject.GetComponent<Stats>();

        if (player != null)
        {
            player.SubstractHealth(Damage);
            gameObject.SetActive(false);
        }

        if ((destroyingMask | (1 << other.gameObject.layer)) == destroyingMask)
            gameObject.SetActive(false);
    }
}
