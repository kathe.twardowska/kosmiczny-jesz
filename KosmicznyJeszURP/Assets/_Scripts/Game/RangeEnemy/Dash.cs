﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dash : MonoBehaviour, IEnemySkill
{
    [SerializeField] float distance;
    [SerializeField] float dashSpeed;

    private Vector3 _endPosition;
    private bool _dash;

    public bool IsDashing() => _dash;

    public void Use()
    {
        var direction = Vector3.zero;

        switch(Random.Range(0,4))
        {
            case 0:
                direction = transform.up;
                break;
            case 1:
                direction = -transform.up;
                break;
            case 2:
                direction = -transform.right;
                break;
            case 3:
                direction = -transform.right;
                break;
        }
        _endPosition = transform.position + direction * distance;
        _dash = true;
    }

    private void Update()
    {
        if (!_dash)
            return;

        transform.position = Vector3.Lerp(transform.position, _endPosition, dashSpeed * Time.deltaTime);

        if (Vector3.Distance(transform.position, _endPosition) < 1)
            _dash = false;
    }
}
