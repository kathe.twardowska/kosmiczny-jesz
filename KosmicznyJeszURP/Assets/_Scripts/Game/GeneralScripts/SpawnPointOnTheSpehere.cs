﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPointOnTheSpehere : MonoBehaviour
{
    SphereCollider _sphereCollider;
    private void Start()
    {
        _sphereCollider = GetComponent<SphereCollider>();
    }
    public Vector3 GetNewSpawnPosition()
    {
        var x = Random.Range(-1f, 1f);
        var y = Random.Range(-1f, 1f);
        var z = Random.Range(-1f, 1f);
        var position = transform.position + new Vector3(x, y, z).normalized * (_sphereCollider.bounds.size/2).x;
        return position;
    }
}
