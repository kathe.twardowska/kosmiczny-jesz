﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public abstract class Enemy : MonoBehaviour, IKillable, IPooledObject
{
    [SerializeField] float maxHealth;
    [SerializeField] float normalDamage;
    [SerializeField] float speed;
    [SerializeField] float range;
    [SerializeField] float attackSpeed;

    private IEnemySkill _enemySkill;
    private float _attackCooldown;
    protected bool RunFromTarget = false;

    private Rigidbody _rigidbody;
    private Transform _transform;

    private Transform _target;
    public Transform Target { get => _target; set => _target = value; }
    public float DistanceToTarget { get => Vector3.Distance(transform.position, _target.position); }

    private EnemyState _state = EnemyState.Stop;
    public EnemyState State { get => _state; set => _state = value; }

    private float _speed;
    public float Speed { get => _speed; set => _speed = value; }
    public void SlowForDuration(float slowPercent, float slowDuration) => StartCoroutine(SlowForDurationEnum(slowPercent, slowDuration));
    private IEnumerator SlowForDurationEnum(float slowPercent, float slowDuration)
    {
        var slowValue = _speed * slowPercent;
        _speed -= slowValue;
        yield return new WaitForSeconds(slowDuration);
        _speed += slowValue;
    }

    private float _health;
    public float Health { get => _health; private set => _health = Mathf.Clamp(_health = value, 0, maxHealth); }
    public void AddHealth(float amount) => Health += amount;
    public void SubstractHealth(float amount)
    {
        Health -= amount;

        if (_health <= 0)
            Die();
    }
    protected virtual void Die()
    {
        gameObject.SetActive(false);
    }

    public float GetMaxHealth() => maxHealth;
    public float GetDamage() => normalDamage;
    public float GetRange() => range;
    public float GetAttackSpeed() => attackSpeed;

    protected virtual void UseAbility() => _enemySkill.Use();

    protected virtual void Start()
    { 
        _rigidbody = GetComponent<Rigidbody>();
        _transform = transform;
        _health = maxHealth;
        _speed = speed;
        _enemySkill = GetComponent<IEnemySkill>();
        _target = GameObject.FindGameObjectWithTag("Player").transform;
    }

    protected virtual void Update() => ProceedState();
    private void ProceedState()
    {
        _attackCooldown += Time.deltaTime;
        IdleAnimation();

        switch (_state)
        {
            case EnemyState.Stop:
                break;
            case EnemyState.Attack:
                BeforeAttackTarget();
                break;
            case EnemyState.FollowTarget:
                FollowTarget();
                break;
            case EnemyState.RunFromTarget:
                RunFromTargetMethod();
                break;

        }
    }
    private void BeforeAttackTarget()
    {
        _transform.LookAt(_target);

        if (DistanceToTarget > range) _state = EnemyState.FollowTarget;
        if (RunFromTarget && DistanceToTarget < range / 2f) _state = EnemyState.RunFromTarget;
        if (_attackCooldown < attackSpeed) return;
        _attackCooldown = 0;
        AttackTarget();
    }
    protected abstract void AttackTarget();
    private void FollowTarget()
    {
        if (_target == null)
            return;

        _rigidbody.velocity = GetVelocity();
        _transform.LookAt(_target);

        var distanceCondition = DistanceToTarget <= range;
        var attackableCondition = _target.GetComponent<IKillable>() != null || _target.GetComponent<Stats>() != null;

        if(distanceCondition && attackableCondition)
        {
            _state = EnemyState.Attack;
            _rigidbody.velocity = Vector3.zero;
        }
    }
    private Vector3 GetVelocity() => GetDirection() * _speed;
    private Vector3 GetDirection() => (_target.position - transform.position).normalized;
    private void RunFromTargetMethod()
    {
        if (_target == null)
            return;

        _rigidbody.velocity = -GetVelocity();
        _transform.LookAt(_target);

        if (DistanceToTarget > range)
            _state = EnemyState.FollowTarget;
    }
    protected virtual void IdleAnimation() { }

    public void OnObjectSpawn() { }
}
