﻿public enum EnemyState
{
    Stop,
    Attack,
    FollowTarget,
    IdleMove,
    RunFromTarget
}
