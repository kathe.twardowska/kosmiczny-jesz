﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Consumable : MonoBehaviour, IPooledObject
{
    private bool _moveUp = true;
    private float _startY;
    private float _time;

    [SerializeField] AnimationCurve movementIdleAnimationCurve;

    public virtual void Eat(Stats player)
    {
        gameObject.SetActive(false);
    }

    public virtual void OnObjectSpawn() { }
    
    protected virtual void Start()
    {
        _startY = transform.position.y;
    }
    protected virtual void Update()
    {
        MoveUpAndDown();
        CheckMovementChange();

        SlowRotation();
    }

    private void MoveUpAndDown()
    {
        if (_moveUp)
            _time += Time.deltaTime * 0.5f;
        else
            _time -= Time.deltaTime * 0.5f;

        transform.position = new Vector3(transform.position.x, _startY + movementIdleAnimationCurve.Evaluate(_time)/2f, transform.position.z);
    }

    private void CheckMovementChange()
    {
        if (_time>=1 && _moveUp)
            _moveUp = false;

        if (_time<=0 && !_moveUp)
            _moveUp = true;
    }

    private void SlowRotation()
    {
        transform.Rotate(0, Time.deltaTime * 30f, 0);
    }
}