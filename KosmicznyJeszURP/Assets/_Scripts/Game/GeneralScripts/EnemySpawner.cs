﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] float distance;
    [SerializeField] float spawnCooldown;

    [SerializeField] int kamikadzeCount;
    [SerializeField] int meleeCount;
    [SerializeField] int rangerCount;
    [SerializeField] int bigCount;

    private int _kamikadze;
    private int _melee;
    private int _range;
    private int _big;

    private float _cooldown;

    private void UpdateCounts()
    {
        _kamikadze = FindObjectsOfType<KamikadzeEnemy>().Length;
        _melee = FindObjectsOfType<MeleeEnemy>().Length;
        _range = FindObjectsOfType<RangeEnemy>().Length;
        _big = FindObjectsOfType<BadassEnemy>().Length;
    }

    private void Start()
    {
        for (int i = 0; i < kamikadzeCount; i++)
        {
            var position = transform.position + new Vector3(Random.Range(-1, 1f), Random.Range(-1, 1f), Random.Range(-1, 1f)).normalized * distance;
            ObjectPooler.instance.SpawnFromPool("Kamikadze", position, Quaternion.identity);
        }

        for (int i = 0; i < meleeCount; i++)
        {
            var position = transform.position + new Vector3(Random.Range(-1, 1f), Random.Range(-1, 1f), Random.Range(-1, 1f)).normalized * distance;
            ObjectPooler.instance.SpawnFromPool("Melee", position, Quaternion.identity);
        }

        for (int i = 0; i < rangerCount; i++)
        {
            var position = transform.position + new Vector3(Random.Range(-1, 1f), Random.Range(-1, 1f), Random.Range(-1, 1f)).normalized * distance;
            ObjectPooler.instance.SpawnFromPool("Range", position, Quaternion.identity);
        }

        for (int i = 0; i < bigCount; i++)
        {
            var position = transform.position + new Vector3(Random.Range(-1, 1f), Random.Range(-1, 1f), Random.Range(-1, 1f)).normalized * distance;
            ObjectPooler.instance.SpawnFromPool("Badass", position, Quaternion.identity);
        }
    }

    private void Update()
    {
        UpdateCounts();

        if(_kamikadze < kamikadzeCount || _melee < meleeCount || _range < rangerCount || _big < bigCount)
        {
            _cooldown += Time.deltaTime;

            if (_cooldown < spawnCooldown)
                return;

            var objectToSpawn = "";
            var position = transform.position + new Vector3(Random.Range(-1, 1f), Random.Range(-1, 1f), Random.Range(-1, 1f)).normalized * distance;

            if (_kamikadze < kamikadzeCount)
                objectToSpawn = "Kamikadze";
            else if (_melee < meleeCount)
                objectToSpawn = "Melee";
            else if (_range < rangerCount)
                objectToSpawn = "Range";
            else if (_big < bigCount)
                objectToSpawn = "Badass";

            ObjectPooler.instance.SpawnFromPool(objectToSpawn, position, Quaternion.identity);

            _cooldown = 0;
        }
    }
}