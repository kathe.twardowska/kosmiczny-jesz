﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    public static CameraShake instance;

    private void Awake()
    {
        instance = this;
    }

    public void Shake(float duration, float magnitude)
    {
        StartCoroutine(ShakeEnum(duration, magnitude));
    }

    private IEnumerator ShakeEnum(float duration, float magnitude)
    {
        var origin = transform.localPosition;

        var eclasped = 0f;

        while(eclasped < duration)
        {
            var x = Random.Range(-1, 1f) * magnitude;
            var y = Random.Range(-1, 1f) * magnitude;

            transform.localPosition = origin + new Vector3(x, y, 0);

            eclasped += Time.deltaTime;

            yield return null;
        }

        transform.localPosition = origin;
    }
}
