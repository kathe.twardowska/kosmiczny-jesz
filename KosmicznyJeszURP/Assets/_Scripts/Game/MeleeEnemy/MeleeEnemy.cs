﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeEnemy : Enemy
{
    protected override void Start()
    {
        base.Start();
        State = EnemyState.FollowTarget;
    }

    protected override void AttackTarget()
    {
        //animacja ataku
        var playerStats = Target.GetComponent<Stats>();
        playerStats.SubstractHealth(GetDamage());
    }
}
