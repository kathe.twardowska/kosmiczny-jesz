﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidSpawner : MonoBehaviour
{
    [SerializeField] float minRange = 22;
    [SerializeField] float maxRange = 40;
    [SerializeField] int minAsteroids;

    private int _asteroidCount = 0;

    private void Start()
    {
        for (int i = 0; i < minAsteroids; i++)
        {
            SpawnAsteroid();
        }

        UpdateCount();
    }

    private void SpawnAsteroid()
    {
        var direction = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f)).normalized;
        var distance = Random.Range(minRange, maxRange);
        var obj = ObjectPooler.instance.SpawnFromPool(Random.Range(0f, 1f) < 0.5f ? "Asteroida" : "Asteroida2", transform.position + direction * distance, Quaternion.identity);
        var playerPosition = transform.position + new Vector3(Random.Range(-20, 20f), Random.Range(-20, 20f), Random.Range(-20, 20f));
        obj.GetComponent<Rigidbody>().velocity = (playerPosition - obj.transform.position).normalized * Random.Range(2, 5f);
    }

    private void UpdateCount()
    {
        _asteroidCount = FindObjectsOfType<Asteroida>().Length;
    }

    private void Update()
    {
        UpdateCount();

        if (_asteroidCount < minAsteroids)
            SpawnAsteroid();
    }
}
