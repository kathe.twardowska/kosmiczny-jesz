﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroida : MonoBehaviour, IKillable
{
    private float _health = 50;
    private Vector3 _rotateDirection;
    private float _rotationSpeed;
    [SerializeField] float damage = 50;
    [SerializeField] float despawnDistance = 60;
    [SerializeField] ParticleSystem destroyParticle;
    private Transform _player;

    public void SubstractHealth(float amount)
    {
        _health -= amount;

        if (_health <= 0)
            Die();
    }

    private void Die()
    {
        Disable();
        CameraShake.instance.Shake(.2f, .1f);
    }

    private void Start()
    {
        _rotateDirection = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f)).normalized;
        _rotationSpeed = Random.Range(10, 30f);
        _player = FindObjectOfType<Stats>().gameObject.transform;
    }

    private void Update()
    {
        transform.Rotate(_rotateDirection * _rotationSpeed * Time.deltaTime);

        if (Vector3.Distance(transform.position, _player.position) > despawnDistance)
            gameObject.SetActive(false);
    }

    private void OnCollisionEnter(Collision collision)
    {
        var player = collision.gameObject.GetComponent<Stats>();
        var asteroid = collision.gameObject.GetComponent<Asteroida>();

        if (player != null)
        {
            player.SubstractHealth(damage);
            Disable();
        }

        if (asteroid != null)
            Disable();
    }

    private void Disable()
    {
        Instantiate(destroyParticle, transform.position, Quaternion.identity);
        gameObject.SetActive(false);
    }
}
