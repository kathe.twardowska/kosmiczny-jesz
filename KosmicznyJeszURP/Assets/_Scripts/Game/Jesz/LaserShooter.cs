﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserShooter : MonoBehaviour
{
    [SerializeField] GameObject laser;
    [SerializeField] Transform leftEye;
    [SerializeField] Transform rightEye;
    [SerializeField] float laserSpeed;

    public float fireRate;
    public float Damage;

    private float _cooldown;

    public void Shoot()
    {
        if (_cooldown < fireRate)
            return;

        _cooldown = 0;

        var leftLaser = InstantiateOnEye(leftEye);
        var rightLaser = InstantiateOnEye(rightEye);

        leftLaser.GetComponent<LaserInstance>().damage = Damage;
        rightLaser.GetComponent<LaserInstance>().damage = Damage;

        var leftLaserRigidbody = GetLaserRigidbody(leftLaser);
        var rightLaserRigidbody = GetLaserRigidbody(rightLaser);

        leftLaserRigidbody.velocity = GetVelocityFromEye(leftEye);
        rightLaserRigidbody.velocity = GetVelocityFromEye(rightEye);
    }

    private GameObject InstantiateOnEye(Transform eye)
    {
        return ObjectPooler.instance.SpawnFromPool("Laser", eye.position, eye.rotation * Quaternion.Euler(90,0,0));
    }

    private Rigidbody GetLaserRigidbody(GameObject laser)
    {
        return laser.GetComponent<Rigidbody>();
    }

    private Vector3 GetVelocityFromEye(Transform eye)
    {
        return eye.transform.forward * laserSpeed;
    }

    private void Update()
    {
        _cooldown += Time.deltaTime;
    }
}
