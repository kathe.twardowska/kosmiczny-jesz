﻿using UnityEngine;

public class Stats : MonoBehaviour
{
    [SerializeField] float maxHealth;
    [SerializeField] float maxEnergy;

    private float _health;
    private float _energy;

    public float Health { get => _health; private set => _health = Mathf.Clamp(_health = value, 0, maxHealth); }
    public float Energy { get => _energy; private set => _energy = Mathf.Clamp(_energy = value, 0, maxEnergy); }

    public void AddHealth(float amount) => Health += amount;
    public void SubstractHealth(float amount) 
    {
        Health -= amount; 
        CameraShake.instance.Shake(.2f, .1f);
    }
    public void AddEnergy(float amount) => Energy += amount;
    public void SubstractEnergy(float amount) => Energy -= amount;

    private void Start()
    {
        _health = maxHealth;
        _energy = maxEnergy;
    }
}
