﻿using System.Collections.Generic;
using UnityEngine;

//Spawner

public class SpawnerManager : MonoBehaviour
{
    public List<Wave> wave;
    public int numberOfSpawningWave;
    public int _enemiesAmount;
    public bool shouldSpawn;
    //dodac wywoływane wave'y w odstepstwie czasu
    private void Update()
    {

        //Pisane By: dawid - ogolnie to jest do debuga. Trzeba zrobić metodę wywłowania się różnych fal, razem z konfiguracją 
        //ilosci przeciwnikow i czestotliwosci
        if (Input.GetKeyDown(KeyCode.U))
        {
            StartCoroutine(wave[numberOfSpawningWave].StartSpawningIteration(_enemiesAmount));
        }
    }

    public void OnEnemiesWaveStart(int waveIndex,int enemiesAmount)
    {
        StartCoroutine(wave[waveIndex].StartSpawningIteration(enemiesAmount));
    }
}
