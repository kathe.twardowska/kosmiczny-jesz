﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class InputController : MonoBehaviour
{
    private LaserShooter _shooter;
    private LightSaberAttack _lightSaber;
    private JetPack _jetPack;
    private void Awake()
    {
        _shooter = GetComponent<LaserShooter>();
        _lightSaber = GetComponent<LightSaberAttack>();
        _jetPack = GetComponent<JetPack>();
    }

    void Update()
    {
        LaserShooterHandler();
        LightSaberHandler();
        JetPackHandler();
    }

    private void LaserShooterHandler()
    {
        if (_shooter == null)
            return;

        if (Input.GetKey(KeyCode.Mouse0))
            _shooter.Shoot();
    }

    private void LightSaberHandler()
    {
        if (_lightSaber == null)
            return;

        if (Input.GetKey(KeyCode.Mouse1))
            _lightSaber.Attack();
        else
            _lightSaber.StopAttack();
    }

    private void JetPackHandler()
    {
        if (_jetPack == null)
            return;

        if(Input.GetKey(KeyCode.Space))
            _jetPack.Fart();
    }
}
