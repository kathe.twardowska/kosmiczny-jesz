﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserInstance : MonoBehaviour, IPooledObject
{
    private Rigidbody _rigidbody;
    [SerializeField] LayerMask enemies;

    [HideInInspector]
    public float damage;

    public void OnObjectSpawn() { }

    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        transform.rotation = Quaternion.LookRotation(transform.forward, transform.up);
    }

    private void OnTriggerEnter(Collider other)
    {
        if ((enemies | (1 << other.gameObject.layer)) == enemies)
        {
            gameObject.SetActive(false);

            var killableObject = other.GetComponent<IKillable>();
            if (killableObject != null)
                killableObject.SubstractHealth(damage);
        }
    }
}