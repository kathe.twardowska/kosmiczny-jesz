﻿using UnityEngine;

public class JetPack : MonoBehaviour
{
    [SerializeField] float maxSpeed;
    [SerializeField] float jetpackForce = 10.0f;
    private ParticleSystem _particle;
    private Rigidbody _body;

    private void Awake()
    {
        _particle = GetComponentInChildren<ParticleSystem>();
        _body = GetComponent<Rigidbody>();
    }
    private void Update()
    {
        if (_body.velocity.magnitude > maxSpeed)
            _body.velocity = _body.velocity.normalized * maxSpeed;
    }

    public void Fart()
    {
        _particle.gameObject.SetActive(true);
        _body.AddForce(transform.forward * Time.deltaTime * jetpackForce);
    }

    public void StopEmmiting() => _particle.gameObject.SetActive(false);
}