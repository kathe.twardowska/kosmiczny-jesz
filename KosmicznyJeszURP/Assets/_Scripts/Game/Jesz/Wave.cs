﻿using Assets._Scripts.Game.Jesz;
using System;
using System.Collections;
using UnityEditor.PackageManager;
using UnityEngine;

[Serializable]
public class Wave : MonoBehaviour
{
    [SerializeField]
    public WavesTags wavesTag;
    private int _enemiesAmountToSpawn;
    SpawnPointOnTheSpehere _spawnPointOnTheSpehere;
    void Start()
    {
        _spawnPointOnTheSpehere = GetComponentInParent<SpawnPointOnTheSpehere>();
    }

    public IEnumerator StartSpawningIteration(int enemiesAmountToSpawn)
    {
        _enemiesAmountToSpawn = 10;
        yield return StartCoroutine(SpawnEnemy());
    }

    private void RepeatingSpawn()
    {
        if (_enemiesAmountToSpawn > 0)
        {
            float randomTime = UnityEngine.Random.Range(1, 5);
            Invoke("SpawnEnemy", randomTime);
            _enemiesAmountToSpawn--;
        }
    }

    IEnumerator SpawnEnemy()
    {
        print(_enemiesAmountToSpawn);

        while (_enemiesAmountToSpawn > 0)
        {
            yield return new WaitForSeconds(UnityEngine.Random.Range(1, 5));
            var randomPos = _spawnPointOnTheSpehere.GetNewSpawnPosition();
            var a = (Quaternion.LookRotation(GameObject.FindGameObjectWithTag("Player").transform.position - randomPos, Vector3.up));
            ObjectPooler.instance.SpawnFromPool(wavesTag.ToString(), randomPos, Quaternion.identity);
            _enemiesAmountToSpawn--;
        }
    }

    public void StopSpawning()
    {
        _enemiesAmountToSpawn = 0;
    }
}