﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightSaberAttack : MonoBehaviour
{
    [SerializeField] Animator animator;
    [SerializeField] LayerMask enemy;

    private float _cooldown;
    public float Damage;
    public float Range = 3.5f;

    public void Attack()
    {
        if (_cooldown < 1f/3f)
            return;

        _cooldown = 0;
        animator.SetInteger("Attack", ChooseAttack());

        var hit = Physics.SphereCastAll(transform.position, 1, transform.forward, Range, enemy);

        for (int i = 0; i < hit.Length; i++)
        {
            hit[i].transform.gameObject.GetComponent<IKillable>().SubstractHealth(Damage);
        }
    }

    public void StopAttack()
    {
        animator.SetInteger("Attack", 0);
    }

    private int ChooseAttack()
    {
        var state = Random.Range(1, 5);

        while(state == animator.GetInteger("Attack"))
            state = Random.Range(1, 5);

        return state;
    }

    private void Update()
    {
        _cooldown += Time.deltaTime;
    }
}
