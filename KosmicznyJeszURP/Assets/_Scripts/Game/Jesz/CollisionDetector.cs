﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionDetector : MonoBehaviour
{
    private Stats playerStats;

    private void Awake()
    {
        playerStats = GetComponent<Stats>();
    }

    private void OnTriggerEnter(Collider collision)
    {
        var consumable = collision.transform.GetComponent<Consumable>();

        if (consumable != null)
            consumable.Eat(playerStats);
    }
}
