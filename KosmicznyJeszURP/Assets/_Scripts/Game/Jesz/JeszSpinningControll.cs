﻿using System.Collections;
using UnityEngine;

public class JeszSpinningControll : MonoBehaviour
{
    [SerializeField] float speed;

    [SerializeField] float lerpSpeed;
    float mouseX = 0;
    float mouseY = 0;

    private void Update()
    {
        mouseX += Input.GetAxis("Mouse X");
        mouseY -= Input.GetAxis("Mouse Y");

        mouseX = Mathf.Lerp(mouseX, 0, lerpSpeed * Time.deltaTime);
        mouseY = Mathf.Lerp(mouseY, 0, lerpSpeed * Time.deltaTime);

        transform.Rotate(new Vector3(mouseY, mouseX, 0) * speed);
    }
}
