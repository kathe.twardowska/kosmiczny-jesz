﻿using UnityEngine;

public class WaveTrigger : MonoBehaviour
{
    private SpawnerManager _spawner;
    public int enemiesAmount;
    public int waveIndex;

    private void Start()
    {
        _spawner = FindObjectOfType<SpawnerManager>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
            _spawner.OnEnemiesWaveStart(waveIndex, enemiesAmount);
    }
}