﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class JeszCollisions : MonoBehaviour
{
    public LayerMask Layers;
    public bool IsColliding;
    
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (1 << collision.gameObject.layer == Layers)
        {
            Debug.Log("menu: jesz grounded");
            IsColliding = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (1 << collision.gameObject.layer == Layers)
        {
            Debug.Log("menu: jesz flies");
            IsColliding = false;
        }
    }
}
