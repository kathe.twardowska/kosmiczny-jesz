﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.Animations;
using UnityEngine;

public class JeszEnvironmentAction : MonoBehaviour
{
    Animator _anim;
    public GameObject sword;
    private void Start()
    {
        _anim = GetComponent<Animator>();
    }
    public void OnGotHelmet()
    {
        _anim.SetTrigger("GotHelmet");
    }
    public void OnGotSword()
    {
        sword.SetActive(true);
    }
    public void OnAlmostGotApple()
    {
        print("almost got apple");
    }
    public void OnGetInsideTube()
    {
        //var a= this.gameObject.AddComponent(typeof(Animator)) as Animator;
        GetComponent<Animator>().runtimeAnimatorController = Resources.Load<RuntimeAnimatorController>("JeszThrowing");
    }
}
