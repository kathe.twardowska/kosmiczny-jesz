﻿using UnityEngine;

public class Item : MonoBehaviour
{

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var helmEntered = collision.gameObject.tag == "Player" && this.gameObject.tag == "Helm";
        var swordEntered = collision.gameObject.tag == "Player" && this.gameObject.tag == "Sword";
        var appleAlmostEntered = collision.gameObject.tag == "Player" && this.gameObject.tag == "Apple";

        if (helmEntered)
        {
            collision.gameObject.GetComponent<JeszEnvironmentAction>().OnGotHelmet();
            this.gameObject.SetActive(false);
        }
        else if (swordEntered)
        {
            collision.gameObject.GetComponent<JeszEnvironmentAction>().OnGotSword();
            this.gameObject.SetActive(false);
        }
        else if (appleAlmostEntered)
        {
            collision.gameObject.GetComponent<JeszEnvironmentAction>().OnAlmostGotApple();
            GetComponent<Animator>().SetTrigger("StartPlaying");
            //this.gameObject.SetActive(false);
        }
    }

    public void TurnOffPlayerMovement()
    {
        GameObject.FindGameObjectWithTag("Player").GetComponent<JeszMenuMovement>().canMove = false;
    }
    public void TurnOnPlayerMovement()
    {
        GameObject.FindGameObjectWithTag("Player").GetComponent<JeszMenuMovement>().canMove = true;

    }
}