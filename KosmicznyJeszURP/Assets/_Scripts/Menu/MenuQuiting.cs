﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuQuiting : MonoBehaviour
{
    public GameObject ExitPanel;

    public void OnExit()
    {
        Application.Quit();
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            OnExitQuestion();
        }
    }

    public void OnExitQuestion()
    {
        ExitPanel.SetActive(true);
    }

    public void OnBack()
    {
        if (ExitPanel != null)
        {
            ExitPanel.SetActive(false);
        }
        else
        {
            Debug.LogError("Panel exit is not attached as Gameobject in MenuManager !!!");
        }
    }


}
