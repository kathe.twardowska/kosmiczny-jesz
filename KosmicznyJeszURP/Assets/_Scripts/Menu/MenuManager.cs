﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public Animator anim;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            StartCoroutine(ChangeSceneAfterAnim());
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            StartCoroutine(ChangeSceneAfterAnim());
        }
    }

    private IEnumerator ChangeSceneAfterAnim()
    {
        anim.SetTrigger("Transistion-Black");
        yield return new WaitForSeconds(0.35f);
        SceneManager.LoadScene(1);
    }

  
}