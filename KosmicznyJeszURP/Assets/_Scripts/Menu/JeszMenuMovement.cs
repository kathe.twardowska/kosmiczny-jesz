﻿using System.Collections;
using UnityEngine;

public class JeszMenuMovement : MonoBehaviour
{
    [SerializeField]
    private float _speed = 50;
    [SerializeField]
    private float _jumpPower = 50;
    [SerializeField]
    private JeszCollisions _jeszCollisions;
    private bool isReadyToJump = true;
    private Rigidbody2D _rb;
    Animator _anim;
    SpriteRenderer _sprite;
    JeszEnvironmentAction _action;
    public bool canMove = true;
    private void Start()
    {
        _anim = GetComponent<Animator>();
        _sprite = GetComponent<SpriteRenderer>();
        _rb = GetComponent<Rigidbody2D>();
        _action = GetComponent<JeszEnvironmentAction>();
        LockCursor();
    }
    private void Update()
    {
        if (!canMove)
            return;
        var horizontalInput = Input.GetAxisRaw("Horizontal");
        var isMoving = horizontalInput != 0;
        _anim.SetBool("IsMoving", isMoving);
        if (isMoving)
        {
            if(horizontalInput < 0)
            {
                _sprite.flipX = true;
                if (_action.sword != null)
                    _action.sword.transform.localScale = new Vector3(-1, 1, 1);

            }
            else if(horizontalInput > 0)
            {
                _sprite.flipX = false;
                if (_action.sword != null)
                    _action.sword.transform.localScale = new Vector3(1, 1, 1);
            }

            transform.position += Vector3.right * Time.deltaTime * _speed * horizontalInput;
        }
    }
    private void LockCursor()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }
    private void FixedUpdate()
    {
        Jump();   
    }

    public void Jump()
    {
        var jumpInput = Input.GetAxis("Jump");

        var isGrounded = _jeszCollisions.IsColliding; 
        if (jumpInput != 0 && isGrounded && isReadyToJump)
        {
            _rb.AddForce(Vector2.up*_jumpPower, ForceMode2D.Impulse);
            StartCoroutine(JumpTimeout());
        }
    }

    IEnumerator JumpTimeout()
    {
        isReadyToJump = false;
        yield return new WaitForSeconds(0.5f);
        isReadyToJump = true;
    }
    
}