﻿using UnityEngine;

public class CameraFloating : MonoBehaviour
{
    public GameObject player;
    public float smoothSpeed = 0.125f;
    public Vector3 offset;

    private void LateUpdate()
    {
        var desiredPos = player.transform.position + offset;
        var a = Mathf.Lerp(transform.position.x, desiredPos.x, smoothSpeed * Time.deltaTime);
        //var smoothedPos = Vector2.Lerp(transform.position, desiredPos, smoothSpeed * Time.deltaTime);
        var pos = new Vector2(a, transform.position.y);

        transform.position = pos;
    }
}