﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    Stats stats;
    public Image image;
    private void Start()
    {
        stats = FindObjectOfType<Stats>();
    }
    private void Update()
    {
        var hp = stats.Health;
        image.fillAmount = (hp / 100f);
        print(hp);
    }
}
